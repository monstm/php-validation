<?php

namespace Samy\Validation\Constant;

/**
 * Simple Validation InvalidArgument implementation.
 */
class ValidationInvalidArgument
{
    // private const EXISTS        = 1;
    private const NOT_EXISTS    = 2;
    // private const EMPTY         = 3;
    // private const NOT_EMPTY     = 4;
    // private const VALID         = 5;
    private const INVALID       = 6;

    private const ERROR             = 0x1000;
    private const METHOD            = 0x2000;
    // private const RULE              = 0x3000;
    // private const VALIDATION        = 0x4000;
    private const BUILDIN           = 0x5000;

    // private const BUILDIN_ATTRIBUTE = self::BUILDIN + 0x0100;
    // private const BUILDIN_ASSERT    = self::BUILDIN + 0x0200;
    private const BUILDIN_EXPECT    = self::BUILDIN + 0x0300;
    private const BUILDIN_ACTUAL    = self::BUILDIN + 0x0400;
    private const BUILDIN_DATA      = self::BUILDIN + 0x0500;
    private const BUILDIN_INTERNAL  = self::BUILDIN + 0x0600;
    // private const BUILDIN_EXTERNAL  = self::BUILDIN + 0x0600;

    public const ERROR_NOT_EXISTS               = self::ERROR + self::NOT_EXISTS;

    public const METHOD_NOT_EXISTS              = self::METHOD + self::NOT_EXISTS;
    public const METHOD_INVALID                 = self::METHOD + self::INVALID;

    public const BUILDIN_EXPECT_INVALID         = self::BUILDIN_EXPECT + self::INVALID;
    public const BUILDIN_ACTUAL_INVALID         = self::BUILDIN_ACTUAL + self::INVALID;
    public const BUILDIN_DATA_NOT_EXISTS        = self::BUILDIN_DATA + self::NOT_EXISTS;
    public const BUILDIN_INTERNAL_NOT_EXISTS    = self::BUILDIN_INTERNAL + self::NOT_EXISTS;
}
