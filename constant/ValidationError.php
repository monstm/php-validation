<?php

namespace Samy\Validation\Constant;

/**
 * Simple Validation Error implementation.
 */
class ValidationError
{
    private const EXPECT_BOOL       = 0x1000;
    private const EXPECT_INT        = 0x2000;
    // private const EXPECT_FLOAT      = 0x3000;
    private const EXPECT_NUMERIC    = 0x4000;
    private const EXPECT_STRING     = 0x5000;
    private const EXPECT_ARRAY      = 0x6000;
    private const EXPECT_MIXED      = 0xF000;

    // private const ACTUAL_BOOL       = 0x0100;
    // private const ACTUAL_INT        = 0x0200;
    // private const ACTUAL_FLOAT      = 0x0300;
    private const ACTUAL_NUMERIC    = 0x0400;
    private const ACTUAL_STRING     = 0x0500;
    private const ACTUAL_ARRAY      = 0x0600;
    private const ACTUAL_MIXED      = 0x0F00;

    public const REQUIRED       = self::EXPECT_BOOL + self::ACTUAL_MIXED + 1;
    public const EMPTY          = self::EXPECT_BOOL + self::ACTUAL_MIXED + 2;
    public const NOT_EMPTY      = self::EXPECT_BOOL + self::ACTUAL_MIXED + 3;

    public const LENGTH         = self::EXPECT_INT + self::ACTUAL_STRING + 1;
    public const LENGTH_MIN     = self::EXPECT_INT + self::ACTUAL_STRING + 2;
    public const LENGTH_MAX     = self::EXPECT_INT + self::ACTUAL_STRING + 3;

    public const SIZE           = self::EXPECT_INT + self::ACTUAL_ARRAY + 1;
    public const SIZE_MIN       = self::EXPECT_INT + self::ACTUAL_ARRAY + 2;
    public const SIZE_MAX       = self::EXPECT_INT + self::ACTUAL_ARRAY + 3;

    public const MIN            = self::EXPECT_NUMERIC + self::ACTUAL_NUMERIC + 1;
    public const MAX            = self::EXPECT_NUMERIC + self::ACTUAL_NUMERIC + 2;

    public const FORMAT         = self::EXPECT_STRING + self::ACTUAL_STRING + 1;
    public const REGEX          = self::EXPECT_STRING + self::ACTUAL_STRING + 2;

    public const TYPE           = self::EXPECT_STRING + self::ACTUAL_MIXED + 1;
    public const NOT_TYPE       = self::EXPECT_STRING + self::ACTUAL_MIXED + 2;
    public const EQUAL          = self::EXPECT_STRING + self::ACTUAL_MIXED + 3;
    public const NOT_EQUAL      = self::EXPECT_STRING + self::ACTUAL_MIXED + 4;

    public const IN             = self::EXPECT_ARRAY + self::ACTUAL_MIXED + 1;
    public const NOT_IN         = self::EXPECT_ARRAY + self::ACTUAL_MIXED + 2;

    public const INSTANCE       = self::EXPECT_MIXED + self::ACTUAL_STRING + 1;
    public const NOT_INSTANCE   = self::EXPECT_MIXED + self::ACTUAL_STRING + 2;

    public const VALUE          = self::EXPECT_MIXED + self::ACTUAL_MIXED + 1;
    public const NOT_VALUE      = self::EXPECT_MIXED + self::ACTUAL_MIXED + 2;
}
