<?php

namespace Samy\Validation\Interface;

/**
 * Describes Error interface.
 */
interface ErrorInterface
{
    /**
     * Retrieves all validation error values.
     *
     * @return array<string,array<string,mixed>>
     */
    public function getErrors(): array;

    /**
     * Checks if a error exists by the given case-sensitive name.
     *
     * @param string $Name The error name.
     * @return bool
     */
    public function hasError(string $Name): bool;

    /**
     * Retrieve a validation error value by the given case-sensitive name.
     *
     * @param string $Name The error name.
     * @return array<string,mixed>
     */
    public function getError(string $Name): array;

    /**
     * Return an instance with the provided value replacing the specified error.
     *
     * @param string $Name The error name.
     * @param string $Message The error message.
     * @param int $Code The error code.
     * @return static
     */
    public function withError(string $Name, string $Message, int $Code = 0): self;

    /**
     * Return an instance without the specified error.
     *
     * @param string $Name The error name.
     * @return static
     */
    public function withoutError(string $Name): self;
}
