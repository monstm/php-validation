<?php

namespace Samy\Validation\Interface;

/**
 * Describes Build-In interface.
 */
interface BuildInInterface
{
    /**
     * Retrieve assert name.
     *
     * @return string
     */
    public function getAssert(): string;

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable;

    /**
     * Retrieve error code.
     *
     * @return int
     */
    public function getErrorCode(): int;

    /**
     * Retrieve error message.
     *
     * @return string
     */
    public function getErrorMessage(): string;
}
