<?php

namespace Samy\Validation\Interface;

/**
 * Describes Method interface.
 */
interface MethodInterface
{
    /**
     * Retrieves all validation method callbacks.
     *
     * @return array<string,callable>
     */
    public function getMethods(): array;

    /**
     * Checks if a method exists by the given case-sensitive name.
     *
     * @param string $Name The method name.
     * @return bool
     */
    public function hasMethod(string $Name): bool;

    /**
     * Retrieve a validation method callback by the given case-sensitive name.
     *
     * @param string $Name The method name.
     * @return ?callable
     */
    public function getMethod(string $Name): ?callable;

    /**
     * Return an instance with the provided callback replacing the specified method.
     *
     * @param string $Name The method name.
     * @param callable $Callback The method callback.
     * @return static
     */
    public function withMethod(string $Name, callable $Callback): self;

    /**
     * Return an instance without the specified method.
     *
     * @param string $Name The method name.
     * @return static
     */
    public function withoutMethod(string $Name): self;
}
