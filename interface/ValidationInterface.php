<?php

namespace Samy\Validation\Interface;

use InvalidArgumentException;
use Samy\Validation\ValidationException;

/**
 * Describes Validation interface.
 */
interface ValidationInterface
{
    /**
     * Check if data is valid.
     *
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @return bool
     */
    public function isValid(array $Data): bool;

    /**
     * Retrieve last error code.
     *
     * @return int
     */
    public function getErrorCode(): int;

    /**
     * Retrieve last error message.
     *
     * @return string
     */
    public function getErrorMessage(): string;

    /**
     * Validate the data using defined rules.
     *
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function validate(array $Data): self;
}
