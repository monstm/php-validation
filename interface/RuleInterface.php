<?php

namespace Samy\Validation\Interface;

/**
 * Describes Rule interface.
 */
interface RuleInterface
{
    /**
     * Retrieves all validation rule values.
     *
     * @return array<string,array<string,mixed>>
     */
    public function getRules(): array;

    /**
     * Checks if a rule exists by the given case-sensitive name.
     *
     * @param string $Name The rule name.
     * @return bool
     */
    public function hasRule(string $Name): bool;

    /**
     * Retrieve a validation rule value by the given case-sensitive name.
     *
     * @param string $Name The rule name.
     * @return array<string,mixed>
     */
    public function getRule(string $Name): array;

    /**
     * Return an instance with the provided value replacing the specified rule.
     *
     * @param string $Name The rule name.
     * @param array<string,mixed> $Rule The rule value.
     * @return static
     */
    public function withRule(string $Name, array $Rule): self;

    /**
     * Return an instance without the specified rule.
     *
     * @param string $Name The rule name.
     * @return static
     */
    public function withoutRule(string $Name): self;
}
