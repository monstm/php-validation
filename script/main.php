<?php

require("consts.php");
require("function.php");

$source = getSource();
generateData(DATA_FILE, $source["actual"]);
generateValidation(VALIDATION_FILE, $source);
