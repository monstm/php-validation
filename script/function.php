<?php

/**
 * @return array<string,array<string,mixed>>
 */
function getSource(): array
{
    $ret = [];
    $scandir = scandir(SOURCE_PATH);

    if (!is_array($scandir)) {
        return $ret;
    }

    foreach ($scandir as $filename) {
        $path = SOURCE_PATH . DIRECTORY_SEPARATOR . $filename;
        if (!is_file($path) || (pathinfo($path, PATHINFO_EXTENSION) != "json")) {
            continue;
        }

        $content = file_get_contents($path);
        if (!is_string($content)) {
            continue;
        }

        $json = json_decode($content, true);
        if (!is_array($json)) {
            continue;
        }

        $ret = array_merge_recursive($json, $ret);
    }

    return $ret;
}

/**
 * @param string $Filename The CSV output filename
 * @param array<string,mixed> $Actual The actual data
 * @return void
 */
function generateData(string $Filename, array $Actual): void
{
    $result = [
        ["attribute", "type", "value"]
    ];

    foreach ($Actual as $attribute => $value) {
        if (!is_string($attribute)) {
            continue;
        }

        $field = [];
        $field[DATA_ATTRIBUTE] = $attribute;
        $field[DATA_TYPE] = getCsvType($value);
        $field[DATA_VALUE] = getCsvValue($value);

        array_push($result, $field);
    }

    writeCsv($Filename, $result);
}

/**
 * @param string $Filename The CSV output filename
 * @param array<string,array<string,mixed>> $Source The source data
 * @return void
 */
function generateValidation(string $Filename, array $Source): void
{
    $result = [
        [
            "id", "assert", "expect", "actual", "rule-type", "rule-value",
            "invalidargument-exception", "invalidargument-code", "invalidargument-message",
            "validation-exception", "validation-code", "validation-message"
        ]
    ];

    enumValidation($Source, function ($Data) use (&$result) {
        $field = [];
        $field[VALIDATION_ID] = "validation-" . count($result);
        $field[VALIDATION_ASSERT] = $Data["assert"];
        $field[VALIDATION_EXPECT] = $Data["expect"];
        $field[VALIDATION_ACTUAL] = $Data["actual"];

        $field[VALIDATION_RULE_TYPE] = getCsvType($Data["rule"]);
        $field[VALIDATION_RULE_VALUE] = getCsvValue($Data["rule"]);

        $field[VALIDATION_INVALIDARGUMENT_EXCEPTION] = getCsvValue($Data["invalidargument-exception"]);
        $field[VALIDATION_INVALIDARGUMENT_CODE] = getCsvValue($Data["invalidargument-code"]);
        $field[VALIDATION_INVALIDARGUMENT_MESSAGE] = $Data["invalidargument-message"];

        $field[VALIDATION_VALIDATION_EXCEPTION] = getCsvValue($Data["validation-exception"]);
        $field[VALIDATION_VALIDATION_CODE] = getCsvValue($Data["validation-code"]);
        $field[VALIDATION_VALIDATION_MESSAGE] = $Data["validation-message"];

        array_push($result, $field);
    });

    writeCsv($Filename, $result);
}

/**
 * @param array<string,array<string,mixed>> $Source The source data
 * @param callable $Callback The source data
 * @return void
 */
function enumValidation(array $Source, callable $Callback): void
{
    foreach ($Source["assert"] as $assert => $config) {
        if (!is_string($assert)) {
            continue;
        }

        if (is_array($config)) {
            foreach ($Source["expect"] as $expect_name => $expect_value) {
                if (!is_string($expect_name)) {
                    continue;
                }

                foreach ($Source["actual"] as $actual_name => $actual_value) {
                    if (!is_string($actual_name)) {
                        continue;
                    }

                    $context = [
                        "attribute" => $actual_name,
                        "assert" => $assert,
                        "expect" => $expect_value,
                        "actual" => $actual_value
                    ];

                    $invalidargument = getInvalidArgument(
                        $config["invalidargument"],
                        $expect_value,
                        $actual_value,
                        $context
                    );

                    $validation = getValidation($config, $expect_name, $actual_name, $context);

                    $Callback([
                        "assert" => $assert,
                        "expect" => $expect_name,
                        "actual" => $actual_name,
                        "rule" => $expect_value,
                        "invalidargument-exception" => $invalidargument["exception"],
                        "invalidargument-code" => $invalidargument["code"],
                        "invalidargument-message" => $invalidargument["message"],
                        "validation-exception" => $validation["exception"],
                        "validation-code" => $validation["code"],
                        "validation-message" => $validation["message"]
                    ]);
                }
            }
        }
    }
}

/**
 * @param array<string,mixed> $Config The invalidargument config
 * @param mixed $Expect The expect value
 * @param mixed $Actual The actual value
 * @param array<string,mixed> $Context The interpolation context
 * @return array<string,mixed>
 */
function getInvalidArgument(array $Config, mixed $Expect, mixed $Actual, mixed $Context): array
{
    $exception = false;
    $code = 0;
    $message = "";

    $invalidarguments = [
        getInvalidType($Config, $Expect, "expect-type", "expect-error-code", "expect-error-message", $Context),
        getInvalidType($Config, $Actual, "actual-type", "actual-error-code", "actual-error-message", $Context),
    ];

    foreach ($invalidarguments as $invalidargument) {
        if ($invalidargument["exception"]) {
            $exception = true;
            $code = $invalidargument["code"];
            $message = $invalidargument["message"];
            break;
        }
    }

    return [
        "exception" => $exception,
        "code" => $code,
        "message" => $message
    ];
}

/**
 * @param array<string,mixed> $Config The invalidargument config
 * @param mixed $Value The value
 * @param string $TypeKey The type key
 * @param string $CodeKey The code key
 * @param string $MessageKey The message key
 * @param array<string,mixed> $Context The interpolation context
 * @return array<string,mixed>
 */
function getInvalidType(
    array $Config,
    mixed $Value,
    string $TypeKey,
    string $CodeKey,
    string $MessageKey,
    mixed $Context
): array {
    $exception = false;
    $code = 0;
    $message = "";

    $value_type = strtolower(gettype($Value));
    $invalid_types = is_array($Config[$TypeKey]) ? $Config[$TypeKey] : [];

    if (in_array($value_type, $invalid_types)) {
        $exception = true;
        $code = $Config[$CodeKey] ?? 0;
        $message_value = $Config[$MessageKey] ?? "";
        $message = is_string($message_value) ? interpolate($message_value, $Context) : "";
    }

    return [
        "exception" => $exception,
        "code" => $code,
        "message" => $message
    ];
}

/**
 * @param array<string,mixed> $Config The root config
 * @param string $Expect The expect name
 * @param string $Actual The actual name
 * @param array<string,mixed> $Context The interpolation context
 * @return array<string,mixed>
 */
function getValidation(array $Config, string $Expect, string $Actual, mixed $Context): array
{
    $exception = true;
    $code = $Config["invalid-code"] ?? 0;
    $invalid_message = $Config["invalid-message"] ?? "";
    $message = (is_string($invalid_message) ? interpolate($invalid_message, $Context) : "");

    if (is_array($Config["valid"])) {
        foreach ($Config["valid"] as $valid) {
            $valid_expect = $valid["expect"] ?? "";
            $valid_actual = $valid["actual"] ?? "";

            if (($valid_expect == $Expect) && ($valid_actual == $Actual)) {
                $exception = false;
                $code = 0;
                $message = "";
                break;
            }
        }
    }

    return [
        "exception" => $exception,
        "code" => $code,
        "message" => $message
    ];
}

/**
 * @param string $message
 * @param array<string,mixed> $context
 *
 * @return string
 */
function interpolate(string $message, array $context = array())
{
    $replace_pairs = array();
    foreach ($context as $key => $data) {
        switch (strtolower(gettype($data))) {
            case "array":
                $encode = json_encode($data);
                $value = (is_string($encode) ? $encode : "[]");
                break;
            default:
                $value = strval($data);
                break;
        }

        $replace_pairs["{{" . $key . "}}"] = $value;
    }

    return strtr($message, $replace_pairs);
}

/**
 * @param mixed $Value The value
 * @return int
 */
function getCsvType(mixed $Value): int
{
    switch (strtolower(gettype($Value))) {
        case "boolean":
            $ret = 1;
            break;
        case "integer":
            $ret = 2;
            break;
        case "double":
            $ret = 3;
            break;
        case "string":
            $ret = 4;
            break;
        case "array":
            $ret = 5;
            break;
        default:
            $ret = 0;
            break;
    }

    return $ret;
}

/**
 * @param mixed $Value The value
 * @return string
 */
function getCsvValue(mixed $Value): string
{
    switch (strtolower(gettype($Value))) {
        case "boolean":
            $ret = ($Value ? "true" : "false");
            break;
        case "integer":
        case "double":
            $ret = strval($Value);
            break;
        case "string":
            $ret = (is_string($Value) ? trim($Value) : strval($Value));
            break;
        case "array":
            $encode = json_encode($Value);
            $ret = (is_string($encode) ? $encode : "[]");
            break;
        default:
            $ret = "null";
            break;
    }

    return $ret;
}

/**
 * @param string $Filename
 * @param array<int,array<int,int|string>> $Data
 * @return void
 */
function writeCsv(string $Filename, array $Data): void
{
    $file = fopen($Filename, "w");
    if ($file) {
        foreach ($Data as $fields) {
            fputcsv($file, $fields);
        }

        fclose($file);
    }
}
