<?php

namespace Samy\Validation;

use Exception;

/**
 * Every Validation exception MUST implement this interface.
 */
class ValidationException extends Exception
{
}
