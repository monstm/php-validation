<?php

namespace Samy\Validation;

use Samy\Validation\Abstract\AbstractValidation;
use Samy\Validation\BuildIn\BuildInEmpty;
use Samy\Validation\BuildIn\BuildInEqual;
use Samy\Validation\BuildIn\BuildInFormat;
use Samy\Validation\BuildIn\BuildInIn;
use Samy\Validation\BuildIn\BuildInInstance;
use Samy\Validation\BuildIn\BuildInLength;
use Samy\Validation\BuildIn\BuildInLengthMax;
use Samy\Validation\BuildIn\BuildInLengthMin;
use Samy\Validation\BuildIn\BuildInMax;
use Samy\Validation\BuildIn\BuildInMin;
use Samy\Validation\BuildIn\BuildInNotEmpty;
use Samy\Validation\BuildIn\BuildInNotEqual;
use Samy\Validation\BuildIn\BuildInNotIn;
use Samy\Validation\BuildIn\BuildInNotInstance;
use Samy\Validation\BuildIn\BuildInNotType;
use Samy\Validation\BuildIn\BuildInNotValue;
use Samy\Validation\BuildIn\BuildInRegex;
use Samy\Validation\BuildIn\BuildInRequired;
use Samy\Validation\BuildIn\BuildInSize;
use Samy\Validation\BuildIn\BuildInSizeMax;
use Samy\Validation\BuildIn\BuildInSizeMin;
use Samy\Validation\BuildIn\BuildInType;
use Samy\Validation\BuildIn\BuildInValue;
use Samy\Validation\Interface\BuildInInterface;

/**
 * Simple Validation implementation.
 */
class Validation extends AbstractValidation
{
    public function __construct()
    {
        $this
            ->buildInMethod(new BuildInRequired())
            ->buildInMethod(new BuildInEmpty())
            ->buildInMethod(new BuildInNotEmpty())
            ->buildInMethod(new BuildInLength())
            ->buildInMethod(new BuildInLengthMin())
            ->buildInMethod(new BuildInLengthMax())
            ->buildInMethod(new BuildInSize())
            ->buildInMethod(new BuildInSizeMin())
            ->buildInMethod(new BuildInSizeMax())
            ->buildInMethod(new BuildInMin())
            ->buildInMethod(new BuildInMax())
            ->buildInMethod(new BuildInFormat())
            ->buildInMethod(new BuildInRegex())
            ->buildInMethod(new BuildInType())
            ->buildInMethod(new BuildInNotType())
            ->buildInMethod(new BuildInEqual())
            ->buildInMethod(new BuildInNotEqual())
            ->buildInMethod(new BuildInIn())
            ->buildInMethod(new BuildInNotIn())
            ->buildInMethod(new BuildInInstance())
            ->buildInMethod(new BuildInNotInstance())
            ->buildInMethod(new BuildInValue())
            ->buildInMethod(new BuildInNotValue());
    }

    /**
     * Build-In Method Factory.
     *
     * @return static
     */
    private function buildInMethod(BuildInInterface $BuildInFactory): self
    {
        return $this
            ->withMethod($BuildInFactory->getAssert(), $BuildInFactory->getMethod())
            ->withError(
                $BuildInFactory->getAssert(),
                $BuildInFactory->getErrorMessage(),
                $BuildInFactory->getErrorCode()
            );
    }
}
