<?php

namespace Test\Inherit;

use Samy\Validation\Abstract\AbstractError;

/**
 * Simple Error implementation.
 */
class Error extends AbstractError
{
}
