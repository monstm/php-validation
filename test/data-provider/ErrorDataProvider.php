<?php

namespace Test\DataProvider;

class ErrorDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve with-error data provider.
     *
     * @return array<array<string,mixed>>
     */
    public static function dataWithError(): array
    {
        return self::csvWithError();
    }

    /**
     * Retrieve without-error data provider.
     *
     * @return array<array<string,mixed>|string>
     */
    public static function dataWithoutError(): array
    {
        $ret = []; // witherrors, name
        $witherrors = self::csvWithError();
        $withouterrors = self::csvWithoutError();

        foreach ($withouterrors as $withouterror) {
            array_push($ret, [
                "witherrors" => $witherrors,
                "name" => $withouterror["name"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve has-error data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string|bool>
     */
    public static function dataHasError(): array
    {
        $ret = []; // witherrors, witherrors, name, expect
        $witherrors = self::csvWithError();
        $withouterrors = self::csvWithoutError();

        foreach (self::csvError() as $error) {
            array_push($ret, [
                "witherrors" => $witherrors,
                "withouterrors" => $withouterrors,
                "name" => $error["name"],
                "expect" => $error["exists"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-error data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string>
     */
    public static function dataGetError(): array
    {
        $ret = []; // witherrors, witherrors, name, expect
        $witherrors = self::csvWithError();
        $withouterrors = self::csvWithoutError();

        foreach (self::csvError() as $error) {
            array_push($ret, [
                "witherrors" => $witherrors,
                "withouterrors" => $withouterrors,
                "name" => $error["name"],
                "expect" => ($error["exists"] ?
                    [
                        "code" => $error["code"],
                        "message" => $error["message"]
                    ] :
                    []
                )
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-errors data provider.
     *
     * @return array<array<string,mixed>|array<string,string>>
     */
    public static function dataGetErrors(): array
    {
        $ret = []; // witherrors, witherrors, expect
        $witherrors = self::csvWithError();
        $withouterrors = self::csvWithoutError();
        $geterrors = [];

        foreach (self::csvError() as $error) {
            if ($error["exists"]) {
                $name = $error["name"];
                $geterrors[$name] = [
                    "code" => $error["code"],
                    "message" => $error["message"]
                ];
            }
        }

        array_push($ret, [
            "witherrors" => $witherrors,
            "withouterrors" => $withouterrors,
            "expect" => $geterrors
        ]);

        return $ret;
    }

    /**
     * Retrieve csv with-error data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvWithError(): array
    {
        $ret = []; // name, message, code

        foreach (self::csv("error-witherror") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "message" => $data["message"],
                "code" => intval($data["code"])
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve csv without-error data provider.
     *
     * @return array<array<string,string>>
     */
    private static function csvWithoutError(): array
    {
        $ret = []; // name

        foreach (self::csv("error-withouterror") as $data) {
            array_push($ret, ["name" => $data["name"]]);
        }

        return $ret;
    }

    /**
     * Retrieve csv error data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvError(): array
    {
        $ret = []; // name, exists, message, code

        foreach (self::csv("error") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "exists" => $data["exists"] == "true",
                "message" => $data["message"],
                "code" => intval($data["code"])
            ]);
        }

        return $ret;
    }
}
