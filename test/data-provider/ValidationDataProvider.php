<?php

namespace Test\DataProvider;

class ValidationDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve validation data provider.
     *
     * @return array<array<string,mixed>|string>
     */
    public static function dataValidation(): array
    {
        $ret = []; // attribute, rule, data, invalid argument, valid, code, message
        $data = self::getValidationData();

        foreach (self::csvValidation() as $validation) {
            $id = $validation["id"];
            $ret[$id] = [
                "attribute" => $validation["attribute"],
                "rule" => $validation["rule"],
                "data" => $data,
                "invalidargument" => $validation["invalidargument"],
                "validation" => $validation["validation"]
            ];
        }

        return $ret;
    }

    /**
     * Retrieve validation data.
     *
     * @return array<string,mixed>
     */
    private static function getValidationData(): array
    {
        $ret = [];

        foreach (self::csv("validation-data") as $data) {
            $attribute = $data["attribute"];
            $ret[$attribute] = self::getCsvValue($data["type"], $data["value"]);
        }

        return $ret;
    }

    /**
     * Retrieve csv validation data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvValidation(): array
    {
        $ret = []; // attribute, assert, expect, invalidargument, validation

        foreach (self::csv("validation") as $data) {
            $rule = [];
            $assert = $data["assert"];
            $rule[$assert] = self::getCsvValue($data["rule-type"], $data["rule-value"]);

            array_push($ret, [
                "id" => $data["id"],
                "attribute" => $data["actual"],
                "rule" => $rule,
                "invalidargument" => [
                    "exception" => ($data["invalidargument-exception"] == "true"),
                    "code" => intval($data["invalidargument-code"]),
                    "message" => $data["invalidargument-message"]
                ],
                "validation" => [
                    "exception" => ($data["validation-exception"] == "true"),
                    "code" => intval($data["validation-code"]),
                    "message" => $data["validation-message"]
                ]
            ]);
        }

        return $ret;
    }
}
