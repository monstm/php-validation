<?php

namespace Test\DataProvider;

use Samy\PhpUnit\AbstractDataProvider as PhpUnitAbstractDataProvider;

abstract class AbstractDataProvider extends PhpUnitAbstractDataProvider
{
    /**
     * Retrieve csv data.
     *
     * @param string $Name The CSV name.
     * @return array<array<string,string>>
     */
    public static function csv(string $Name): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "csv" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".csv";

        return parent::csv($filename);
    }

    /**
     * Retrieve CSV value.
     *
     * @param string $Type
     * @param string $Value
     * @return mixed
     */
    protected static function getCsvValue(string $Type, string $Value): mixed
    {
        switch (intval($Type)) {
            case 1:
                $ret = ($Value == "true");
                break;
            case 2:
                $ret = intval($Value);
                break;
            case 3:
                $ret = floatval($Value);
                break;
            case 4:
                $ret = trim($Value);
                break;
            case 5:
                $ret = json_decode($Value, true);
                break;
            default:
                $ret = null;
                break;
        }

        return $ret;
    }
}
