<?php

namespace Test\DataProvider;

class MethodDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve with-method data provider.
     *
     * @return array<array<string,mixed>>
     */
    public static function dataWithMethod(): array
    {
        return self::csvWithMethod();
    }

    /**
     * Retrieve without-method data provider.
     *
     * @return array<array<string,mixed>|string>
     */
    public static function dataWithoutMethod(): array
    {
        $ret = []; // withmethods, name
        $withmethods = self::csvWithMethod();
        $withoutmethods = self::csvWithoutMethod();

        foreach ($withoutmethods as $withoutmethod) {
            array_push($ret, [
                "withmethods" => $withmethods,
                "name" => $withoutmethod["name"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve has-method data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string|bool>
     */
    public static function dataHasMethod(): array
    {
        $ret = []; // withmethods, withmethods, name, expect
        $withmethods = self::csvWithMethod();
        $withoutmethods = self::csvWithoutMethod();

        foreach (self::csvMethod() as $method) {
            array_push($ret, [
                "withmethods" => $withmethods,
                "withoutmethods" => $withoutmethods,
                "name" => $method["name"],
                "expect" => $method["exists"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-method data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string>
     */
    public static function dataGetMethod(): array
    {
        $ret = []; // withmethods, withmethods, name, expect
        $withmethods = self::csvWithMethod();
        $withoutmethods = self::csvWithoutMethod();

        foreach (self::csvMethod() as $method) {
            array_push($ret, [
                "withmethods" => $withmethods,
                "withoutmethods" => $withoutmethods,
                "name" => $method["name"],
                "expect" => ($method["exists"] ? $method["callback"] : null)
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-methods data provider.
     *
     * @return array<array<string,mixed>|array<string,string>>
     */
    public static function dataGetMethods(): array
    {
        $ret = []; // withmethods, withmethods, expect
        $withmethods = self::csvWithMethod();
        $withoutmethods = self::csvWithoutMethod();
        $getmethods = [];

        foreach (self::csvMethod() as $method) {
            if ($method["exists"]) {
                $name = $method["name"];
                $getmethods[$name] = $method["callback"];
            }
        }

        array_push($ret, [
            "withmethods" => $withmethods,
            "withoutmethods" => $withoutmethods,
            "expect" => $getmethods
        ]);

        return $ret;
    }

    /**
     * Retrieve csv with-method data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvWithMethod(): array
    {
        $ret = []; // name, callback

        foreach (self::csv("method-withmethod") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "callback" => self::getMethodCallback()
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve csv without-method data provider.
     *
     * @return array<array<string,string>>
     */
    private static function csvWithoutMethod(): array
    {
        $ret = []; // name

        foreach (self::csv("method-withoutmethod") as $data) {
            array_push($ret, ["name" => $data["name"]]);
        }

        return $ret;
    }

    /**
     * Retrieve csv method data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvMethod(): array
    {
        $ret = []; // name, exists, callback

        foreach (self::csv("method") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "exists" => $data["exists"] == "true",
                "callback" => self::getMethodCallback()
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    private static function getMethodCallback(): callable
    {
        return function (): bool {
            return true;
        };
    }
}
