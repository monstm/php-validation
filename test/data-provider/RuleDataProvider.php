<?php

namespace Test\DataProvider;

class RuleDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve with-rule data provider.
     *
     * @return array<array<string,mixed>>
     */
    public static function dataWithRule(): array
    {
        return self::csvWithRule();
    }

    /**
     * Retrieve without-rule data provider.
     *
     * @return array<array<string,mixed>|string>
     */
    public static function dataWithoutRule(): array
    {
        $ret = []; // withrules, name
        $withrules = self::csvWithRule();
        $withoutrules = self::csvWithoutRule();

        foreach ($withoutrules as $withoutrule) {
            array_push($ret, [
                "withrules" => $withrules,
                "name" => $withoutrule["name"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve has-rule data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string|bool>
     */
    public static function dataHasRule(): array
    {
        $ret = []; // withrules, withrules, name, expect
        $withrules = self::csvWithRule();
        $withoutrules = self::csvWithoutRule();

        foreach (self::csvRule() as $rule) {
            array_push($ret, [
                "withrules" => $withrules,
                "withoutrules" => $withoutrules,
                "name" => $rule["name"],
                "expect" => $rule["exists"]
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-rule data provider.
     *
     * @return array<array<string,mixed>|array<string,string>|string>
     */
    public static function dataGetRule(): array
    {
        $ret = []; // withrules, withrules, name, expect
        $withrules = self::csvWithRule();
        $withoutrules = self::csvWithoutRule();

        foreach (self::csvRule() as $rule) {
            array_push($ret, [
                "withrules" => $withrules,
                "withoutrules" => $withoutrules,
                "name" => $rule["name"],
                "expect" => ($rule["exists"] ? $rule["rule"] : [])
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve get-rules data provider.
     *
     * @return array<array<string,mixed>|array<string,string>>
     */
    public static function dataGetRules(): array
    {
        $ret = []; // withrules, withrules, expect
        $withrules = self::csvWithRule();
        $withoutrules = self::csvWithoutRule();
        $getrules = [];

        foreach (self::csvRule() as $rule) {
            if ($rule["exists"]) {
                $name = $rule["name"];
                $getrules[$name] = $rule["rule"];
            }
        }

        array_push($ret, [
            "withrules" => $withrules,
            "withoutrules" => $withoutrules,
            "expect" => $getrules
        ]);

        return $ret;
    }

    /**
     * Retrieve csv with-rule data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvWithRule(): array
    {
        $ret = []; // name, rule

        foreach (self::csv("rule-withrule") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "rule" => self::getRuleData($data)
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve csv without-rule data provider.
     *
     * @return array<array<string,string>>
     */
    private static function csvWithoutRule(): array
    {
        $ret = []; // name

        foreach (self::csv("rule-withoutrule") as $data) {
            array_push($ret, ["name" => $data["name"]]);
        }

        return $ret;
    }

    /**
     * Retrieve csv rule data provider.
     *
     * @return array<array<string,mixed>>
     */
    private static function csvRule(): array
    {
        $ret = []; // name, exists, rule

        foreach (self::csv("rule") as $data) {
            array_push($ret, [
                "name" => $data["name"],
                "exists" => $data["exists"] == "true",
                "rule" => self::getRuleData($data)
            ]);
        }

        return $ret;
    }

    /**
     * Retrieve rule data.
     *
     * @param array<string,string> $Data
     * @return array<string,mixed>
     */
    private static function getRuleData(array $Data): array
    {
        $ret = [];

        for ($pointer = 1; $pointer <= 3; $pointer++) {
            $key = "assert-" . $pointer;

            if ($Data[$key] == "true") {
                $ret[$key] = self::getCsvValue($Data[$key . "-type"], $Data[$key . "-value"]);
            }
        }

        return $ret;
    }
}
