<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Error;

class ErrorTest extends AbstractTestCase
{
    /** @var Error */
    protected $error;

    protected function setUp(): void
    {
        $this->error = new Error();
    }

    /**
     * Test with-error.
     *
     * @dataProvider \Test\DataProvider\ErrorDataProvider::dataWithError
     * @param string $Name
     * @param string $Message
     * @param int $Code
     * @return void
     */
    public function testWithError(string $Name, string $Message, int $Code): void
    {
        $this->assertInstanceOf(Error::class, $this->error->WithError($Name, $Message, $Code));
    }

    /**
     * Test without-error.
     *
     * @dataProvider \Test\DataProvider\ErrorDataProvider::dataWithoutError
     * @param array<array<string,mixed>> $InitWithError
     * @param string $Name
     * @return void
     */
    public function testWithoutError(array $InitWithError, string $Name): void
    {
        $this->initWithError($InitWithError);

        $this->assertInstanceOf(Error::class, $this->error->WithoutError($Name));
    }

    /**
     * Test has-error.
     *
     * @dataProvider \Test\DataProvider\ErrorDataProvider::dataHasError
     * @param array<array<string,mixed>> $InitWithError
     * @param array<array<string,string>> $InitWithoutError
     * @param string $Name
     * @param bool $Expect
     * @return void
     */
    public function testHasError(array $InitWithError, array $InitWithoutError, string $Name, bool $Expect): void
    {
        $this->initWithError($InitWithError);
        $this->initWithoutError($InitWithoutError);

        $this->assertSame($Expect, $this->error->hasError($Name));
    }

    /**
     * Test get-error.
     *
     * @dataProvider \Test\DataProvider\ErrorDataProvider::dataGetError
     * @param array<array<string,mixed>> $InitWithError
     * @param array<array<string,string>> $InitWithoutError
     * @param string $Name
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testGetError(array $InitWithError, array $InitWithoutError, string $Name, array $Expect): void
    {
        $this->initWithError($InitWithError);
        $this->initWithoutError($InitWithoutError);

        $this->assertSame($Expect, $this->error->getError($Name));
    }

    /**
     * Test get-errors.
     *
     * @dataProvider \Test\DataProvider\ErrorDataProvider::dataGetErrors
     * @param array<array<string,mixed>> $InitWithError
     * @param array<array<string,string>> $InitWithoutError
     * @param array<string,array<string,mixed>> $Expect
     * @return void
     */
    public function testGetErrors(array $InitWithError, array $InitWithoutError, array $Expect): void
    {
        $this->initWithError($InitWithError);
        $this->initWithoutError($InitWithoutError);

        $this->assertSame($Expect, $this->error->getErrors());
    }

    /**
     * initialize with-error.
     *
     * @param array<array<string,mixed>> $Errors
     * @return void
     */
    private function initWithError(array $Errors): void
    {
        foreach ($Errors as $error) {
            $this->assertInstanceOf(
                Error::class,
                $this->error->WithError(
                    strval($error["name"]),
                    strval($error["message"]),
                    intval($error["code"])
                )
            );
        }
    }

    /**
     * initialize without-error.
     *
     * @param array<array<string,string>> $Errors
     * @return void
     */
    private function initWithoutError(array $Errors): void
    {
        foreach ($Errors as $error) {
            $this->assertInstanceOf(
                Error::class,
                $this->error->WithoutError($error["name"])
            );
        }
    }
}
