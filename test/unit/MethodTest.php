<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Method;

class MethodTest extends AbstractTestCase
{
    /** @var Method */
    protected $method;

    protected function setUp(): void
    {
        $this->method = new Method();
    }

    /**
     * Test with-method.
     *
     * @dataProvider \Test\DataProvider\MethodDataProvider::dataWithMethod
     * @param string $Name
     * @param callable $Callback
     * @return void
     */
    public function testWithMethod(string $Name, callable $Callback): void
    {
        $this->assertInstanceOf(Method::class, $this->method->WithMethod($Name, $Callback));
    }

    /**
     * Test without-method.
     *
     * @dataProvider \Test\DataProvider\MethodDataProvider::dataWithoutMethod
     * @param array<array<string,mixed>> $InitWithMethod
     * @param string $Name
     * @return void
     */
    public function testWithoutMethod(array $InitWithMethod, string $Name): void
    {
        $this->initWithMethod($InitWithMethod);

        $this->assertInstanceOf(Method::class, $this->method->WithoutMethod($Name));
    }

    /**
     * Test has-method.
     *
     * @dataProvider \Test\DataProvider\MethodDataProvider::dataHasMethod
     * @param array<array<string,mixed>> $InitWithMethod
     * @param array<array<string,string>> $InitWithoutMethod
     * @param string $Name
     * @param bool $Expect
     * @return void
     */
    public function testHasMethod(array $InitWithMethod, array $InitWithoutMethod, string $Name, bool $Expect): void
    {
        $this->initWithMethod($InitWithMethod);
        $this->initWithoutMethod($InitWithoutMethod);

        $this->assertSame($Expect, $this->method->hasMethod($Name));
    }

    /**
     * Test get-method.
     *
     * @dataProvider \Test\DataProvider\MethodDataProvider::dataGetMethod
     * @param array<array<string,mixed>> $InitWithMethod
     * @param array<array<string,string>> $InitWithoutMethod
     * @param string $Name
     * @param ?callable $Expect
     * @return void
     */
    public function testGetMethod(
        array $InitWithMethod,
        array $InitWithoutMethod,
        string $Name,
        ?callable $Expect
    ): void {
        $this->initWithMethod($InitWithMethod);
        $this->initWithoutMethod($InitWithoutMethod);

        $this->assertSame(is_callable($Expect), is_callable($this->method->getMethod($Name)));
    }

    /**
     * Test get-methods.
     *
     * @dataProvider \Test\DataProvider\MethodDataProvider::dataGetMethods
     * @param array<array<string,mixed>> $InitWithMethod
     * @param array<array<string,string>> $InitWithoutMethod
     * @param array<string,array<string,mixed>> $Expect
     * @return void
     */
    public function testGetMethods(array $InitWithMethod, array $InitWithoutMethod, array $Expect): void
    {
        $this->initWithMethod($InitWithMethod);
        $this->initWithoutMethod($InitWithoutMethod);

        foreach ($this->method->getMethods() as $assert => $method) {
            $this->assertArrayHasKey($assert, $Expect);
            $this->assertSame(is_callable($Expect[$assert]), is_callable($method));
        }
    }

    /**
     * initialize with-method.
     *
     * @param array<array<string,mixed>> $Methods
     * @return void
     */
    private function initWithMethod(array $Methods): void
    {
        foreach ($Methods as $method) {
            if (is_callable($method["callback"])) {
                $this->assertInstanceOf(
                    Method::class,
                    $this->method->WithMethod(
                        strval($method["name"]),
                        $method["callback"]
                    )
                );
            }
        }
    }

    /**
     * initialize without-method.
     *
     * @param array<array<string,string>> $Methods
     * @return void
     */
    private function initWithoutMethod(array $Methods): void
    {
        foreach ($Methods as $method) {
            $this->assertInstanceOf(
                Method::class,
                $this->method->WithoutMethod($method["name"])
            );
        }
    }
}
