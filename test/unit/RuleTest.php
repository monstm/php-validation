<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Rule;

class RuleTest extends AbstractTestCase
{
    /** @var Rule */
    protected $rule;

    protected function setUp(): void
    {
        $this->rule = new Rule();
    }

    /**
     * Test with-rule.
     *
     * @dataProvider \Test\DataProvider\RuleDataProvider::dataWithRule
     * @param string $Name
     * @param array<string,mixed> $Rule
     * @return void
     */
    public function testWithRule(string $Name, array $Rule): void
    {
        $this->assertInstanceOf(Rule::class, $this->rule->WithRule($Name, $Rule));
    }

    /**
     * Test without-rule.
     *
     * @dataProvider \Test\DataProvider\RuleDataProvider::dataWithoutRule
     * @param array<array<string,mixed>> $InitWithRule
     * @param string $Name
     * @return void
     */
    public function testWithoutRule(array $InitWithRule, string $Name): void
    {
        $this->initWithRule($InitWithRule);

        $this->assertInstanceOf(Rule::class, $this->rule->WithoutRule($Name));
    }

    /**
     * Test has-rule.
     *
     * @dataProvider \Test\DataProvider\RuleDataProvider::dataHasRule
     * @param array<array<string,mixed>> $InitWithRule
     * @param array<array<string,string>> $InitWithoutRule
     * @param string $Name
     * @param bool $Expect
     * @return void
     */
    public function testHasRule(array $InitWithRule, array $InitWithoutRule, string $Name, bool $Expect): void
    {
        $this->initWithRule($InitWithRule);
        $this->initWithoutRule($InitWithoutRule);

        $this->assertSame($Expect, $this->rule->hasRule($Name));
    }

    /**
     * Test get-rule.
     *
     * @dataProvider \Test\DataProvider\RuleDataProvider::dataGetRule
     * @param array<array<string,mixed>> $InitWithRule
     * @param array<array<string,string>> $InitWithoutRule
     * @param string $Name
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testGetRule(array $InitWithRule, array $InitWithoutRule, string $Name, array $Expect): void
    {
        $this->initWithRule($InitWithRule);
        $this->initWithoutRule($InitWithoutRule);

        $this->assertSame($Expect, $this->rule->getRule($Name));
    }

    /**
     * Test get-rules.
     *
     * @dataProvider \Test\DataProvider\RuleDataProvider::dataGetRules
     * @param array<array<string,mixed>> $InitWithRule
     * @param array<array<string,string>> $InitWithoutRule
     * @param array<string,array<string,mixed>> $Expect
     * @return void
     */
    public function testGetRules(array $InitWithRule, array $InitWithoutRule, array $Expect): void
    {
        $this->initWithRule($InitWithRule);
        $this->initWithoutRule($InitWithoutRule);

        $this->assertSame($Expect, $this->rule->getRules());
    }

    /**
     * initialize with-rule.
     *
     * @param array<array<string,mixed>> $Rules
     * @return void
     */
    private function initWithRule(array $Rules): void
    {
        foreach ($Rules as $rule) {
            $this->assertInstanceOf(
                Rule::class,
                $this->rule->WithRule(
                    strval($rule["name"]),
                    /* @phpstan-ignore-next-line */
                    $rule["rule"]
                )
            );
        }
    }

    /**
     * initialize without-rule.
     *
     * @param array<array<string,string>> $Rules
     * @return void
     */
    private function initWithoutRule(array $Rules): void
    {
        foreach ($Rules as $rule) {
            $this->assertInstanceOf(
                Rule::class,
                $this->rule->WithoutRule($rule["name"])
            );
        }
    }
}
