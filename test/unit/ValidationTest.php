<?php

namespace Test\Unit;

use InvalidArgumentException;
use Samy\PhpUnit\AbstractTestCase;
use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

class ValidationTest extends AbstractTestCase
{
    /** @var Validation */
    protected $validation;

    protected function setUp(): void
    {
        $this->validation = new Validation();
    }

    /**
     * Test validation.
     *
     * @dataProvider \Test\DataProvider\ValidationDataProvider::dataValidation
     * @param string $Attribute
     * @param array<array<string,mixed>> $Rule
     * @param array<string,mixed> $Data
     * @param array<string,mixed> $InvalidArgument
     * @param array<string,mixed> $InvalidArgument
     * @param array<string,mixed> $Validation
     * @return void
     */
    public function testValidation(
        string $Attribute,
        array $Rule,
        array $Data,
        array $InvalidArgument,
        array $Validation
    ): void {
        $this->assertInstanceOf(Validation::class, $this->validation->withRule($Attribute, $Rule));

        // if (isset($Rule["empty"])) {
            // $this->printError(json_encode([
            //     "Attribute" => $Attribute,
            //     "Rule" => $Rule,
            //     "Data" => $Data,
            //     "InvalidArgument" => $InvalidArgument,
            //     "Validation" => $Validation
            // ], JSON_PRETTY_PRINT));
        // }
        if ($InvalidArgument["exception"]) {
            $this->expectException(InvalidArgumentException::class);

            if (is_int($InvalidArgument["code"])) {
                $this->expectExceptionCode($InvalidArgument["code"]);
            }

            if (is_string($InvalidArgument["message"])) {
                $this->expectExceptionMessage($InvalidArgument["message"]);
            }
        }

        $this->assertSame(!$Validation["exception"], $this->validation->isValid($Data));
        $this->assertSame($Validation["code"], $this->validation->getErrorCode());
        $this->assertSame($Validation["message"], $this->validation->getErrorMessage());

        $this->assertInstanceOf(Validation::class, $this->validation->withoutRule($Attribute));
    }

    /**
     * Test validate.
     *
     * @dataProvider \Test\DataProvider\ValidationDataProvider::dataValidation
     * @param string $Attribute
     * @param array<array<string,mixed>> $Rule
     * @param array<string,mixed> $Data
     * @param array<string,mixed> $InvalidArgument
     * @param array<string,mixed> $InvalidArgument
     * @param array<string,mixed> $Validation
     * @return void
     */
    public function testValidate(
        string $Attribute,
        array $Rule,
        array $Data,
        array $InvalidArgument,
        array $Validation
    ): void {
        $this->assertInstanceOf(Validation::class, $this->validation->withRule($Attribute, $Rule));

        if ($InvalidArgument["exception"]) {
            $this->expectException(InvalidArgumentException::class);

            if (is_int($InvalidArgument["code"])) {
                $this->expectExceptionCode($InvalidArgument["code"]);
            }

            if (is_string($InvalidArgument["message"])) {
                $this->expectExceptionMessage($InvalidArgument["message"]);
            }
        } elseif ($Validation["exception"]) {
            $this->expectException(ValidationException::class);

            if (is_int($Validation["code"])) {
                $this->expectExceptionCode($Validation["code"]);
            }

            if (is_string($Validation["message"])) {
                $this->expectExceptionMessage($Validation["message"]);
            }
        }

        $this->assertInstanceOf(Validation::class, $this->validation->validate($Data));
    }
}
