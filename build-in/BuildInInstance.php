<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Instance implementation.
 */
class BuildInInstance extends AbstractBuildIn
{
    protected $assert = "instance";
    protected $error_code = ValidationError::INSTANCE;
    protected $error_message = "The '{{attribute}}' must be a/an {{expect}}.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["string"],
                $Parameter["assert"] . " assertation only accepts string value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            $ret = false;
            foreach (explode("|", $expect) as $instance) {
                if (is_a($actual, $instance)) {
                    $ret = true;
                    break;
                }
            }

            return $ret;
        };
    }
}
