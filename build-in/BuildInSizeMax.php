<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Size Max implementation.
 */
class BuildInSizeMax extends AbstractBuildIn
{
    protected $assert = "size-max";
    protected $error_code = ValidationError::SIZE_MAX;
    protected $error_message = "The '{{attribute}}' must be less or equal to {{expect}} elements.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this
                ->guardInvalidType(
                    $expect,
                    ["integer"],
                    $Parameter["assert"] . " assertation only accepts integer value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardInvalidType(
                    $actual,
                    ["array"],
                    $Parameter["assert"] . " attribute only accepts array value.",
                    ValidationInvalidArgument::BUILDIN_ACTUAL_INVALID
                );

            return $expect >= count($actual);
        };
    }
}
