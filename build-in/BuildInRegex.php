<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Regex implementation.
 */
class BuildInRegex extends AbstractBuildIn
{
    protected $assert = "regex";
    protected $error_code = ValidationError::REGEX;
    protected $error_message = "The '{{attribute}}' has invalid '{{expect}}' regex pattern.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this
                ->guardInvalidType(
                    $expect,
                    ["string"],
                    $Parameter["assert"] . " assertation only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardInvalidType(
                    $actual,
                    ["string"],
                    $Parameter["assert"] . " attribute only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_ACTUAL_INVALID
                );

            $ret = @preg_match($expect, $actual);

            return is_int($ret) && ($ret == 1);
        };
    }
}
