<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In In implementation.
 */
class BuildInIn extends AbstractBuildIn
{
    protected $assert = "in";
    protected $error_code = ValidationError::IN;
    protected $error_message = "The '{{attribute}}' must exists in {{expect}} list.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["array"],
                $Parameter["assert"] . " assertation only accepts array value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            return in_array($actual, $expect);
        };
    }
}
