<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Not-Equal implementation.
 */
class BuildInNotEqual extends AbstractBuildIn
{
    protected $assert = "not-equal";
    protected $error_code = ValidationError::NOT_EQUAL;
    protected $error_message = "The '{{attribute}}' must not equals with '{{expect}}' value.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];
            $data = $Parameter["data"];

            $this
                ->guardInvalidType(
                    $expect,
                    ["string"],
                    $Parameter["assert"] . " assertation only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardNonExists(
                    $data,
                    $expect,
                    $Parameter["assert"] . " assertation is not exists: " . $expect,
                    ValidationInvalidArgument::BUILDIN_DATA_NOT_EXISTS
                );

            return $actual != $data[$expect];
        };
    }
}
