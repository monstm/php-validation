<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Type implementation.
 */
class BuildInType extends AbstractBuildIn
{
    protected $assert = "type";
    protected $error_code = ValidationError::TYPE;
    protected $error_message = "The '{{attribute}}' must be a/an {{expect}}.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["string"],
                $Parameter["assert"] . " assertation only accepts string value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            $types = explode("|", strtolower($expect));
            $type = strtolower(gettype($actual));

            return in_array($type, $types);
        };
    }
}
