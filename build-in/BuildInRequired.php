<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Required implementation.
 */
class BuildInRequired extends AbstractBuildIn
{
    protected $assert = "required";
    protected $error_code = ValidationError::REQUIRED;
    protected $error_message = "The '{{attribute}}' is required.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $attribute = $Parameter["attribute"];
            $expect = $Parameter["expect"];
            $data = $Parameter["data"];

            $this->guardInvalidType(
                $expect,
                ["boolean"],
                $Parameter["assert"] . " assertation only accepts boolean value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            $ret = !$expect;
            if ($expect && array_key_exists($attribute, $data)) {
                $ret = true;
            }

            return $ret;
        };
    }
}
