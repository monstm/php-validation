<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;

/**
 * Simple Build-In Not-Value implementation.
 */
class BuildInNotValue extends AbstractBuildIn
{
    protected $assert = "not-value";
    protected $error_code = ValidationError::NOT_VALUE;
    protected $error_message = "The '{{attribute}}' must not '{{expect}}'.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            return $expect !== $actual;
        };
    }
}
