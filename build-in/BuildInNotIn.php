<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Not-In implementation.
 */
class BuildInNotIn extends AbstractBuildIn
{
    protected $assert = "not-in";
    protected $error_code = ValidationError::NOT_IN;
    protected $error_message = "The '{{attribute}}' must not exists in {{expect}} list.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["array"],
                $Parameter["assert"] . " assertation only accepts array value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            return !in_array($actual, $expect);
        };
    }
}
