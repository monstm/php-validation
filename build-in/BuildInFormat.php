<?php

namespace Samy\Validation\BuildIn;

use DateTime;
use InvalidArgumentException;
use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Format implementation.
 */
class BuildInFormat extends AbstractBuildIn
{
    protected $assert = "format";
    protected $error_code = ValidationError::FORMAT;
    protected $error_message = "The '{{attribute}}' has invalid '{{expect}}' format.";

    private const FORMAT_FILTERVAR = 1;
    private const FORMAT_DATETIME = 2;

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];
            $metadata = $this->formatMetadata();

            $this
                ->guardInvalidType(
                    $expect,
                    ["string"],
                    $Parameter["assert"] . " assertation only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardInvalidType(
                    $actual,
                    ["string"],
                    $Parameter["assert"] . " attribute only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_ACTUAL_INVALID
                )
                ->guardNonExists(
                    $metadata,
                    $expect,
                    $Parameter["assert"] . " assertation format is not exists.",
                    ValidationInvalidArgument::BUILDIN_INTERNAL_NOT_EXISTS
                );

            $data = $metadata[$expect];
            $format = $data["format"] ?? "";
            switch ($format) {
                case self::FORMAT_FILTERVAR:
                    $filter_type = $data["filter_type"] ?? FILTER_DEFAULT;
                    $filter_option = $data["filter_option"] ?? 0;
                    $ret = $this->formatFilterVal($actual, intval($filter_type), intval($filter_option));
                    break;
                case self::FORMAT_DATETIME:
                    $date_format = $data["date_format"] ?? "Y-m-d H:i:s";
                    $ret = $this->formatDateVal($actual, strval($date_format));
                    break;
                default:
                    $ret = false;
                    throw new InvalidArgumentException("unknown '" . $format . "' format.");
            }

            return $ret;
        };
    }

    /**
     * Retrieve format metadata.
     *
     * @return array<string,array<string,mixed>>
     */
    private function formatMetadata(): array
    {
        return [
            "domain" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_DOMAIN
            ],
            "email" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_EMAIL
            ],
            "url" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_URL
            ],
            "ip" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_IP
            ],
            "ip4" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_IP,
                "filter_option" => FILTER_FLAG_IPV4
            ],
            "ip6" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_IP,
                "filter_option" => FILTER_FLAG_IPV6
            ],
            "mac" => [
                "format" => self::FORMAT_FILTERVAR,
                "filter_type" => FILTER_VALIDATE_MAC
            ],
            "date" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => "Y-m-d"
            ],
            "datetime" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => "Y-m-d H:i:s"
            ],
            "date-atom" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_ATOM
            ],
            "date-cookie" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_COOKIE
            ],
            "date-rfc822" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC822
            ],
            "date-rfc850" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC850
            ],
            "date-rfc1036" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC1036
            ],
            "date-rfc1123" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC1123
            ],
            "date-rfc7231" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC7231
            ],
            "date-rfc2822" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC2822
            ],
            "date-rfc3339" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC3339
            ],
            "date-rfc3339-expanded" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RFC3339_EXTENDED
            ],
            "date-rss" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_RSS
            ],
            "date-w3c" => [
                "format" => self::FORMAT_DATETIME,
                "date_format" => DATE_W3C
            ]
        ];
    }

    /**
     * Retrieve filter_val validation format.
     *
     * @param string $Value
     * @param int $Filter
     * @param int $Option
     * @return bool
     */
    private function formatFilterVal(string $Value, int $Filter, int $Option): bool
    {
        return boolval(filter_var($Value, $Filter, $Option));
    }

    /**
     * Retrieve date validation format.
     *
     * @param string $Value
     * @param string $Format
     * @return bool
     */
    private function formatDateVal(string $Value, string $Format): bool
    {
        $datetime = DateTime::createFromFormat($Format, $Value);
        return ($datetime && ($datetime->format($Format) === $Value));
    }
}
