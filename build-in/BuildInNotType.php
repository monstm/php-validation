<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Not-Type implementation.
 */
class BuildInNotType extends AbstractBuildIn
{
    protected $assert = "not-type";
    protected $error_code = ValidationError::NOT_TYPE;
    protected $error_message = "The '{{attribute}}' must not a/an {{expect}}.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["string"],
                $Parameter["assert"] . " assertation only accepts string value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            $types = explode("|", strtolower($expect));
            $type = strtolower(gettype($actual));

            return !in_array($type, $types);
        };
    }
}
