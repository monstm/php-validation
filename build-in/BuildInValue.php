<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;

/**
 * Simple Build-In Value implementation.
 */
class BuildInValue extends AbstractBuildIn
{
    protected $assert = "value";
    protected $error_code = ValidationError::VALUE;
    protected $error_message = "The '{{attribute}}' must be '{{expect}}'.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            return $expect === $actual;
        };
    }
}
