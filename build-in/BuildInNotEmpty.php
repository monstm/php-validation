<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Not Empty implementation.
 */
class BuildInNotEmpty extends AbstractBuildIn
{
    protected $assert = "not-empty";
    protected $error_code = ValidationError::NOT_EMPTY;
    protected $error_message = "The '{{attribute}}' must not empty.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this->guardInvalidType(
                $expect,
                ["boolean"],
                $Parameter["assert"] . " assertation only accepts boolean value.",
                ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
            );

            $ret = !$expect;
            if ($expect && !empty($actual)) {
                $ret = true;
            }

            return $ret;
        };
    }
}
