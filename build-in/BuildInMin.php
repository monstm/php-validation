<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Min implementation.
 */
class BuildInMin extends AbstractBuildIn
{
    protected $assert = "min";
    protected $error_code = ValidationError::MIN;
    protected $error_message = "The '{{attribute}}' must be greater or equal to {{expect}}.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this
                ->guardInvalidType(
                    $expect,
                    ["integer", "double"],
                    $Parameter["assert"] . " assertation only accepts number value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardInvalidType(
                    $actual,
                    ["integer", "double"],
                    $Parameter["assert"] . " attribute only accepts number value.",
                    ValidationInvalidArgument::BUILDIN_ACTUAL_INVALID
                );

            return $expect <= $actual;
        };
    }
}
