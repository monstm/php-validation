<?php

namespace Samy\Validation\BuildIn;

use Samy\Validation\Abstract\AbstractBuildIn;
use Samy\Validation\Constant\ValidationError;
use Samy\Validation\Constant\ValidationInvalidArgument;

/**
 * Simple Build-In Length Max implementation.
 */
class BuildInLengthMax extends AbstractBuildIn
{
    protected $assert = "length-max";
    protected $error_code = ValidationError::LENGTH_MAX;
    protected $error_message = "The '{{attribute}}' must be less or equal to {{expect}} characters.";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    public function getMethod(): callable
    {
        return function (array $Parameter): bool {
            $expect = $Parameter["expect"];
            $actual = $Parameter["actual"];

            $this
                ->guardInvalidType(
                    $expect,
                    ["integer"],
                    $Parameter["assert"] . " assertation only accepts integer value.",
                    ValidationInvalidArgument::BUILDIN_EXPECT_INVALID
                )
                ->guardInvalidType(
                    $actual,
                    ["string"],
                    $Parameter["assert"] . " attribute only accepts string value.",
                    ValidationInvalidArgument::BUILDIN_ACTUAL_INVALID
                );

            return $expect >= strlen($actual);
        };
    }
}
