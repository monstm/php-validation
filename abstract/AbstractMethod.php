<?php

namespace Samy\Validation\Abstract;

use Samy\Validation\Interface\MethodInterface;

/**
 * This is a simple Method implementation that other Method can inherit from.
 */
abstract class AbstractMethod extends AbstractError implements MethodInterface
{
    /** @var array<string,callable> */
    private $methods = [];

    /**
     * Retrieves all validation method callbacks.
     *
     * @return array<string,callable>
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    /**
     * Checks if a method exists by the given case-sensitive name.
     *
     * @param string $Name The method name.
     * @return bool
     */
    public function hasMethod(string $Name): bool
    {
        return isset($this->methods[$Name]);
    }

    /**
     * Retrieve a validation method callback by the given case-sensitive name.
     *
     * @param string $Name The method name.
     * @return ?callable
     */
    public function getMethod(string $Name): ?callable
    {
        return ($this->hasMethod($Name) ? $this->methods[$Name] : null);
    }

    /**
     * Return an instance with the provided callback replacing the specified method.
     *
     * @param string $Name The method name.
     * @param callable $Callback The method callback.
     * @return static
     */
    public function withMethod(string $Name, callable $Callback): self
    {
        $this->methods[$Name] = $Callback;

        return $this;
    }

    /**
     * Return an instance without the specified method.
     *
     * @param string $Name The method name.
     * @return static
     */
    public function withoutMethod(string $Name): self
    {
        if ($this->hasMethod($Name)) {
            unset($this->methods[$Name]);
        }

        return $this;
    }
}
