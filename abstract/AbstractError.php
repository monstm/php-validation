<?php

namespace Samy\Validation\Abstract;

use Samy\Validation\Interface\ErrorInterface;

/**
 * This is a simple Error implementation that other Error can inherit from.
 */
abstract class AbstractError implements ErrorInterface
{
    /** @var array<string,array<string,mixed>> */
    private $errors = [];

    /**
     * Retrieves all validation error values.
     *
     * @return array<string,array<string,mixed>>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Checks if a error exists by the given case-sensitive name.
     *
     * @param string $Name The error name.
     * @return bool
     */
    public function hasError(string $Name): bool
    {
        return isset($this->errors[$Name]);
    }

    /**
     * Retrieve a validation error value by the given case-sensitive name.
     *
     * @param string $Name The error name.
     * @return array<string,mixed>
     */
    public function getError(string $Name): array
    {
        return ($this->hasError($Name) ? $this->errors[$Name] : []);
    }

    /**
     * Return an instance with the provided value replacing the specified error.
     *
     * @param string $Name The error name.
     * @param string $Message The error message.
     * @param int $Code The error code.
     * @return static
     */
    public function withError(string $Name, string $Message, int $Code = 0): self
    {
        $this->errors[$Name] = [
            "code" => $Code,
            "message" => trim($Message)
        ];

        return $this;
    }

    /**
     * Return an instance without the specified error.
     *
     * @param string $Name The error name.
     * @return static
     */
    public function withoutError(string $Name): self
    {
        if ($this->hasError($Name)) {
            unset($this->errors[$Name]);
        }

        return $this;
    }
}
