<?php

namespace Samy\Validation\Abstract;

use InvalidArgumentException;
use Samy\Validation\ValidationException;
use Samy\Validation\Constant\ValidationInvalidArgument;
use Samy\Validation\Interface\ValidationInterface;

/**
 * This is a simple Validation implementation that other Validation can inherit from.
 */
abstract class AbstractValidation extends AbstractRule implements ValidationInterface
{
    private const ASSERT_MANDATORY = "required";

    /** @var int */
    private $error_code = 0;

    /** @var string */
    private $error_message = "";

    /**
     * Check if data is valid.
     *
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @return bool
     */
    public function isValid(array $Data): bool
    {
        return $this->verifyMandatoryRule($Data) && $this->verifyOptionalRule($Data);
    }

    /**
     * Verify mandatory rule.
     *
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @return bool
     */
    private function verifyMandatoryRule(array $Data): bool
    {
        $ret = true;
        $assert = self::ASSERT_MANDATORY;
        $method = $this->getAssertationCallback($assert);

        foreach ($this->getRules() as $attribute => $rule) {
            if (!array_key_exists($assert, $rule)) {
                continue;
            }

            $expect = $rule[$assert];
            $actual = $Data[$attribute] ?? null;
            $result = $this->executeAssertationCallback($method, $attribute, $assert, $expect, $actual, $Data);

            if (!$result) {
                $ret = false;
                break;
            }
        }

        return $ret;
    }

    /**
     * Verify optional rule.
     *
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @return bool
     */
    private function verifyOptionalRule(array $Data): bool
    {
        $ret = true;

        foreach ($this->getRules() as $attribute => $rule) {
            if (!array_key_exists($attribute, $Data)) {
                continue;
            }

            $actual = $Data[$attribute];
            foreach ($rule as $assert => $expect) {
                if ($assert == self::ASSERT_MANDATORY) {
                    continue;
                }

                $method = $this->getAssertationCallback($assert);
                $result = $this->executeAssertationCallback($method, $attribute, $assert, $expect, $actual, $Data);

                if (!$result) {
                    $ret = false;
                    break 2;
                }
            }
        }

        return $ret;
    }

    /**
     * Retrieve assertation callback.
     *
     * @param string $Assert The assert name.
     * @throws InvalidArgumentException If error.
     * @return callable
     */
    private function getAssertationCallback(string $Assert): callable
    {
        if (!$this->hasError($Assert)) {
            throw new InvalidArgumentException(
                "Validation error for '" . $Assert . "' is not exists",
                ValidationInvalidArgument::ERROR_NOT_EXISTS
            );
        }

        if (!$this->hasMethod($Assert)) {
            throw new InvalidArgumentException(
                "Validation method for '" . $Assert . "' is not exists",
                ValidationInvalidArgument::METHOD_NOT_EXISTS
            );
        }

        $ret = $this->getMethod($Assert);
        if (!is_callable($ret)) {
            throw new InvalidArgumentException(
                "Validation method '" . $Assert . "' is not a function",
                ValidationInvalidArgument::METHOD_INVALID
            );
        }

        return $ret;
    }

    /**
     * Execute assertation callback.
     *
     * @param callable $Method The assertation method.
     * @param string $Attribute The attribute name.
     * @param string $Assert The assert name.
     * @param mixed $Expect The expect value.
     * @param mixed $Actual The actual value.
     * @param array<string,mixed> $Data The data to be validated.
     * @throws InvalidArgumentException If error.
     * @return bool
     */
    private function executeAssertationCallback(
        callable $Method,
        string $Attribute,
        string $Assert,
        mixed $Expect,
        mixed $Actual,
        array $Data
    ): bool {
        $ret = call_user_func($Method, [
            "attribute" => $Attribute,
            "assert" => $Assert,
            "expect" => $Expect,
            "actual" => $Actual,
            "data" => $Data
        ]);

        if (!is_bool($ret)) {
            throw new InvalidArgumentException(
                "Result method '" . $Assert . "' is not a boolean",
                ValidationInvalidArgument::METHOD_INVALID
            );
        }

        if (!$ret) {
            $error = $this->getError($Assert);
            $this->error_code = intval($error["code"]);
            $this->error_message = $this->interpolate(
                strval($error["message"]),
                [
                    "attribute" => $Attribute,
                    "assert" => $Assert,
                    "expect" => $Expect,
                    "actual" => $Actual
                ]
            );
        }

        return $ret;
    }

    /**
     * Interpolates context values into the message placeholders.
     *
     * @param string $message
     * @param array<string,mixed> $context
     * @return string
     */
    protected function interpolate(string $message, array $context = array())
    {
        $replace_pairs = array();
        foreach ($context as $key => $data) {
            switch (strtolower(gettype($data))) {
                case "array":
                    $encode = json_encode($data);
                    $value = (is_string($encode) ? $encode : "[]");
                    break;
                case "object":
                    $value = (is_object($data) ? get_class($data) : "[object]");
                    break;
                default:
                    $value = strval($data);
                    break;
            }

            $replace_pairs["{{" . $key . "}}"] = $value;
        }

        return strtr($message, $replace_pairs);
    }

    /**
     * Retrieve last error code.
     *
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->error_code;
    }

    /**
     * Retrieve last error message.
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->error_message;
    }

    /**
     * Validate the data using defined rules.
     *
     * @param array<string,mixed> $Data The data to be validated.
     *
     * @throws InvalidArgumentException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function validate(array $Data): self
    {
        if (!$this->isValid($Data)) {
            throw new ValidationException($this->getErrorMessage(), $this->getErrorCode());
        }

        return $this;
    }
}
