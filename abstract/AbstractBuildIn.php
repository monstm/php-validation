<?php

namespace Samy\Validation\Abstract;

use InvalidArgumentException;
use Samy\Validation\Interface\BuildInInterface;

/**
 * This is a simple BuildIn implementation that other BuildIn can inherit from.
 */
abstract class AbstractBuildIn implements BuildInInterface
{
    /** @var string */
    protected $assert = "";

    /** @var int */
    protected $error_code = 0;

    /** @var string */
    protected $error_message = "";

    /**
     * Retrieve method callback.
     *
     * @return callable
     */
    abstract public function getMethod(): callable;

    /**
     * Retrieve assert name.
     *
     * @return string
     */
    public function getAssert(): string
    {
        return $this->assert;
    }

    /**
     * Retrieve error code.
     *
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->error_code;
    }

    /**
     * Retrieve error message.
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->error_message;
    }

    /**
     * Guard an instance from invalid type of value.
     *
     * @param mixed $Value The value.
     * @param array<string> $Types The expect types.
     * @param string $ErrorMessage The error message.
     * @param int $ErrorCode The error code.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function guardInvalidType(mixed $Value, array $Types, string $ErrorMessage, int $ErrorCode): self
    {
        $type = strtolower(gettype($Value));

        if (!in_array(strtolower(gettype($Value)), $Types)) {
            throw new InvalidArgumentException($ErrorMessage, $ErrorCode);
        }

        return $this;
    }

    /**
     * Guard an instance from nonexists of data.
     *
     * @param array<mixed> $Data The data.
     * @param string $Key The key.
     * @param string $ErrorMessage The error message.
     * @param int $ErrorCode The error code.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function guardNonExists(array $Data, string $Key, string $ErrorMessage, int $ErrorCode): self
    {
        if (!array_key_exists($Key, $Data)) {
            throw new InvalidArgumentException($ErrorMessage, $ErrorCode);
        }

        return $this;
    }
}
