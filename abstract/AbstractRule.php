<?php

namespace Samy\Validation\Abstract;

use Samy\Validation\Interface\RuleInterface;

/**
 * This is a simple Rule implementation that other Rule can inherit from.
 */
abstract class AbstractRule extends AbstractMethod implements RuleInterface
{
    /** @var array<string,array<string,mixed>> */
    private $rules = [];

    /**
     * Retrieves all validation rule values.
     *
     * @return array<string,array<string,mixed>>
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * Checks if a rule exists by the given case-sensitive name.
     *
     * @param string $Name The rule name.
     * @return bool
     */
    public function hasRule(string $Name): bool
    {
        return isset($this->rules[$Name]);
    }

    /**
     * Retrieve a validation rule value by the given case-sensitive name.
     *
     * @param string $Name The rule name.
     * @return array<string,mixed>
     */
    public function getRule(string $Name): array
    {
        return ($this->hasRule($Name) ? $this->rules[$Name] : []);
    }

    /**
     * Return an instance with the provided value replacing the specified rule.
     *
     * @param string $Name The rule name.
     * @param array<string,mixed> $Rule The rule value.
     * @return static
     */
    public function withRule(string $Name, array $Rule): self
    {
        $this->rules[$Name] = $Rule;

        return $this;
    }

    /**
     * Return an instance without the specified rule.
     *
     * @param string $Name The rule name.
     * @return static
     */
    public function withoutRule(string $Name): self
    {
        if ($this->hasRule($Name)) {
            unset($this->rules[$Name]);
        }

        return $this;
    }
}
