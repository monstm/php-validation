# Methods

Validation method is the process used to confirm
that the procedure employed for a specific test is suitable for its intended use.

---

## Method Interface

Describes Method interface.

### getMethods

Retrieves all validation method values.

```php
$methods = $validation->getMethods();
```

### hasMethod

Checks if a method exists by the given case-sensitive name.

```php
$has_method = $validation->hasMethod($name);
```

### getMethod

Retrieve a validation method callback by the given case-sensitive name.

```php
$callback = $validation->getMethod($name);
```

### withMethod

Return an instance with the provided callback replacing the specified method.

```php
$callback = function(mixed $expect, mixed $actual, array $data): bool{
    ...
};

$validation = $validation->withMethod($name, $callback);
```

### withoutMethod

Return an instance without the specified method.

```php
$validation = $validation->withoutMethod($name);
```

---

## Build-In Methods

| Name       | Expect  | Actual  | Description                                                     |
| ---------- | :-----: | :-----: | --------------------------------------------------------------- |
| required   |  bool   |  mixed  | Makes the actual value required.                                |
| empty      |  bool   |  mixed  | Makes the actual value require an empty value.                  |
| not-empty  |  bool   |  mixed  | Makes the actual value require not an empty value.              |
| length     |   int   | string  | Makes the actual value require a given length.                  |
| length-min |   int   | string  | Makes the actual value require a given minimum length.          |
| length-max |   int   | string  | Makes the actual value require a given maximum length.          |
| size       |   int   |  array  | Makes the actual value require a given size.                    |
| size-min   |   int   |  array  | Makes the actual value require a given minimum size.            |
| size-max   |   int   |  array  | Makes the actual value require a given maximum size.            |
| min        | numeric | numeric | Makes the actual value require a given minimum value.           |
| max        | numeric | numeric | Makes the actual value require a given maximum value.           |
| format     | string  | string  | Makes the actual value require a given format.                  |
| regex      | string  | string  | Makes the actual value match with the given expression.         |
| type       | string  |  mixed  | Makes the actual value require a given type.                    |
| not-type   | string  |  mixed  | Makes the actual value require not a given type.                |
| equal      | string  |  mixed  | Makes the actual value to be the same as another data value.    |
| not-equal  | string  |  mixed  | Makes the actual value to be different with another data value. |
| in         |  array  |  mixed  | Makes the actual value to be present in list.                   |
| not-in     |  array  |  mixed  | Makes the actual value to be absent in list.                    |
| value      |  mixed  |  mixed  | Makes the actual value require a given value.                   |
| not-value  |  mixed  |  mixed  | Makes the actual value require not a given value.               |

Possible values for **type** are:
* null
* boolean
* integer
* double
* string
* array
* object
* resource
* unknown type

Possible values for **format** are:
* domain
* email
* url
* ip
* ip4
* ip6
* mac
* date
* datetime
* date-atom
* date-cookie
* date-rfc822
* date-rfc850
* date-rfc1036
* date-rfc1123
* date-rfc7231
* date-rfc2822
* date-rfc3339
* date-rfc3339-expanded
* date-rss
* date-w3c
