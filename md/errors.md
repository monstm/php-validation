# Errors

Validation error can contain an error code and message to display
when the rule returns a value of "False" due to an invalid value.

---

## Error Interface

Describes Error interface.

### getErrors

Retrieves all validation error values.

```php
$errors = $validation->getErrors();
```

### hasError

Checks if an error exists by the given case-sensitive name.

```php
$has_error = $validation->hasError($name);
```

### getError

Retrieve a validation error value by the given case-sensitive name.

```php
$error = $validation->getError($name);
```

### withError

Return an instance with the provided value replacing the specified error.

```php
$name = "length-min";
$message = "value '{{actual}}' for attribute '{{attribute}}' require '{{expect}}' on assertation '{{assert}}'.";
$code = 123;

$validation = $validation->withError($name, $message, $code);
```

| Message   | Description           |
| --------- | --------------------- |
| attribute | The data attribute.   |
| assert    | The assertation name. |
| expect    | The expect value.     |
| actual    | The actual value.     |

### withoutError

Return an instance without the specified error.

```php
$validation = $validation->withoutError($name);
```
