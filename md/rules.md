# Rules

Validation rules verify that the data meets the standard specifications.
A validation rule can contain a formula or expression that evaluates the data and returns a value of "True" or "False".

---

## Rule Interface

Describes Rule interface.

### getRules

Retrieves all validation rule values.

```php
$rules = $validation->getRules();
```

### hasRule

Checks if a rule exists by the given case-sensitive name.

```php
$has_rule = $validation->hasRule($name);
```

### getRule

Retrieve a validation rule value by the given case-sensitive name.

```php
$rule = $validation->getRule($name);
```

### withRule

Return an instance with the provided value replacing the specified rule.

```php
$rule = [
    "required" => true,
    "type" => "string",
    "length" => 24,
    "format" => "url"
];

$validation = $validation->withRule($name, $rule);
```

### withoutRule

Return an instance without the specified rule.

```php
$validation = $validation->withoutRule($name);
```
