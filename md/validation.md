# Validation

This is a simple way to implement guard and fail fast concepts quickly then spend time on more important things.

---

## Validation Instance

Simple Validation Implementation.

```php
$validation = new \Samy\Validation\Validation();
```

---

## Validation Interface

Describes Validation interface.

### isValid

Check if data is valid.
Throw InvalidArgumentException if error.

```php
$is_valid = $validation->isValid($data);
```

### getErrorCode

Retrieve last error code.

```php
$error_code = $validation->getErrorCode();
```

### getErrorMessage

Retrieve last error message.

```php
$error_message = $validation->getErrorMessage();
```

### validate

Validate the data using defined rules.
Throw InvalidArgumentException if error.
Throw ValidationException if invalid.

```php
$validation = $validation->validate($data);
```
