# PHP Validation

[
	![](https://badgen.net/packagist/v/samy/validation/latest)
	![](https://badgen.net/packagist/license/samy/validation)
	![](https://badgen.net/packagist/dt/samy/validation)
	![](https://badgen.net/packagist/favers/samy/validation)
](https://packagist.org/packages/samy/validation)

Useful Guards and Fail Fast concepts.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/validation
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-validation>
* User Manual: <https://monstm.gitlab.io/php-validation/>
* Documentation: <https://monstm.alwaysdata.net/php-validation/>
* Issues: <https://gitlab.com/monstm/php-validation/-/issues>
